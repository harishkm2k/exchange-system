package com.credit.suisse.stock;

import com.credit.suisse.domain.Direction;
import com.credit.suisse.domain.Order;
import com.credit.suisse.domain.RIC;
import com.credit.suisse.domain.Status;
import com.credit.suisse.utils.StockExchangeUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by harish on 30/10/15.
 */
public class StockExchange {

    ArrayList<Order> orders = new ArrayList<Order>();

    public void addOrder(Order order) {
        orders.add(order);
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    /**
     * When an order is added, it should be compared against existing open orders to see whether it can be matched.
     * Two orders match if they have opposing directions, matching RICs and quantities,
     * and if the sell price is less than or equal to the buy price
     *
     * @param newOrder
     * @return
     */
    public boolean checkOrderMatch(Order newOrder) {
        ArrayList<Order> equalMatchingOrders = filterEqualMatchingOrders(newOrder);
        if (equalMatchingOrders.size() > 0) {
            // If there are multiple matching orders at the same price for a new order,
            // it should be matched against the earliest matching existing orders
            Order earliestOrder = equalMatchingOrders.get(0);
            // Executed orders are removed from the set of open orders against which new orders can be matched
            System.out.println(">>>> Matching Earliest Order " + earliestOrder);
            StockExchangeUtils.changeOrderStatusAsExecuted(newOrder, earliestOrder);
            StockExchangeUtils.changeExecutionPrice(newOrder, earliestOrder);
            return true;
        }

        ArrayList<Order> otherMatchingOrders = filterOtherMatchingOrders(newOrder);
        if (otherMatchingOrders.size() > 1) {
            if (newOrder.getDirection().equals(Direction.SELL)) {
                // If there are multiple matching orders at different prices for a new sell order,
                // it should be matched against the order with the highest price
                Collections.sort(otherMatchingOrders, StockExchangeUtils.getPriceComparator());
                Order highestPriceOrder = otherMatchingOrders.get(otherMatchingOrders.size() - 1);
                System.out.println(">>> Match is : " + highestPriceOrder);
                // Executed orders are removed from the set of open orders against which new orders can be matched
                StockExchangeUtils.changeOrderStatusAsExecuted(newOrder, highestPriceOrder);
                // When two orders are matched they are said to be ‘executed’,
                // and the price at which they are executed (the execution price) is the price of the newly added order
                StockExchangeUtils.changeExecutionPrice(newOrder, highestPriceOrder);
                return true;
            }

            if (newOrder.getDirection().equals(Direction.BUY)) {
                // If there are multiple matching orders at different prices for a new buy order,
                // it should be matched against the order with the lowest price
                Collections.sort(otherMatchingOrders, StockExchangeUtils.getPriceComparator());
                Order lowestPriceOrder = otherMatchingOrders.get(0);
                System.out.println(">>> Match is : " + lowestPriceOrder);
                // Executed orders are removed from the set of open orders against which new orders can be matched
                StockExchangeUtils.changeOrderStatusAsExecuted(newOrder, lowestPriceOrder);
                // When two orders are matched they are said to be ‘executed’,
                // and the price at which they are executed (the execution price) is the price of the newly added order
                StockExchangeUtils.changeExecutionPrice(newOrder, lowestPriceOrder);
                return true;
            }
        }
        return false;
    }


    public ArrayList<Order> filterEqualMatchingOrders(Order newOrder) {
        ArrayList<Order> matchingEqualOrders = new ArrayList<Order>();
        for (Order order : orders) {
            if (StockExchangeUtils.orderIsOpen(order)) {
                if (StockExchangeUtils.ricMatches(order, newOrder)) {
                    if (StockExchangeUtils.oppositeDirections(order, newOrder)) {
                        if (StockExchangeUtils.quantitiesMatches(order, newOrder)) {
                            if (StockExchangeUtils.sellPriceIsEqualToBuyPrice(order, newOrder)) {
                                matchingEqualOrders.add(order);
                                continue;
                            }
                        }
                    }
                }
            }
        }
        return matchingEqualOrders;
    }


    public ArrayList<Order> filterOtherMatchingOrders(Order newOrder) {
        ArrayList<Order> otherMatchingOrders = new ArrayList<Order>();
        for (Order order : orders) {
            if (StockExchangeUtils.orderIsOpen(order)) {
                if (StockExchangeUtils.ricMatches(order, newOrder)) {
                    if (StockExchangeUtils.oppositeDirections(order, newOrder)) {
                        if (StockExchangeUtils.quantitiesMatches(order, newOrder)) {
                            if (StockExchangeUtils.sellPriceIsLessThanBuyPrice(order, newOrder)) {
                                otherMatchingOrders.add(order);
                                continue;
                            }
                        }
                    }
                }
            }
        }
        return otherMatchingOrders;
    }

    /**
     * Provide open interest for a given RIC and direction
     * Open interest is the total quantity of all open orders for the given RIC and direction at each price point
     *
     * @param ric
     * @param buy
     * @return
     */
    public Map<BigDecimal, Integer> provideOpenInterestForGivenRicAndDirection(RIC ric, Direction buy) {
        Map<BigDecimal, Integer> totalQuantityOfOpenOrders = new HashMap<BigDecimal, Integer>();
        for (Order order : orders) {
            if (order.getRic().equals(ric)) {
                if (order.getStatus().equals(Status.OPEN)) {
                    if (order.getDirection().equals(buy)) {
                        if (totalQuantityOfOpenOrders.containsKey(order.getPrice())) {
                            Integer quantity = totalQuantityOfOpenOrders.get(order.getPrice());
                            totalQuantityOfOpenOrders.put(order.getPrice(), new Integer(quantity + 1));
                        } else {
                            totalQuantityOfOpenOrders.put(order.getPrice(), new Integer(1));
                        }
                    }
                }
            }
        }
        System.out.println("Open Interest = " + totalQuantityOfOpenOrders);
        return totalQuantityOfOpenOrders;
    }

    /**
     * Provide the average execution price for a given RIC
     * The average execution price is the average price per unit of all executions for the given RIC
     *
     * @param ric
     * @return
     */
    public BigDecimal averageExecutionPriceForAGivenRIC(RIC ric) {
        double totalPrice = 0;
        int totalQuantity = 0;
        for (Order order : orders) {
            if (order.getRic().equals(ric)) {
                if (order.getStatus().equals(Status.EXECUTED)) {
                    totalPrice = totalPrice + (order.getExecutionPrice().doubleValue() * order.getQuantity());
                    totalQuantity = totalQuantity + order.getQuantity();
                }
            }
        }
        BigDecimal averageExecutionPrice = new BigDecimal(totalPrice / totalQuantity).setScale(2, RoundingMode.CEILING);
        System.out.println("Average Execution Price = " + averageExecutionPrice);
        return averageExecutionPrice;
    }


    /**
     * Provide executed quantity for a given RIC and user
     * Executed quantity is the sum of quantities of executed orders for the given RIC and user.
     * The quantity of sell orders should be negated
     *
     * @param ric
     * @param userName
     * @return
     */
    public int findExecutedQuantityForAnRICAndAnUser(RIC ric, String userName) {
        int totalBuyQuantity = 0;
        int totalSellQuantity = 0;
        for (Order order : orders) {
            if (order.getRic().equals(ric)) {
                if (order.getUser().getName().equals(userName)) {
                    if (order.getStatus().equals(Status.EXECUTED)) {
                        if (order.getDirection().equals(Direction.BUY)) {
                            totalBuyQuantity = totalBuyQuantity + order.getQuantity();
                        }
                        if (order.getDirection().equals(Direction.SELL)) {
                            totalSellQuantity = totalSellQuantity + order.getQuantity();
                        }
                    }

                }
            }
        }

        int executedQuantity = totalBuyQuantity - totalSellQuantity;
        System.out.println("Executed Quantity : " + executedQuantity);
        return executedQuantity;
    }

}