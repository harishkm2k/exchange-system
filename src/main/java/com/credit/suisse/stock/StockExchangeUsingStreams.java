package com.credit.suisse.stock;

import com.credit.suisse.domain.Direction;
import com.credit.suisse.domain.Order;
import com.credit.suisse.domain.RIC;
import com.credit.suisse.domain.Status;
import com.credit.suisse.utils.StockExchangeUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by harish on 30/10/15.
 */
public class StockExchangeUsingStreams {

    ArrayList<Order> orders = new ArrayList<Order>();

    public void addOrder(Order order) {
        orders.add(order);
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    /**
     * When an order is added, it should be compared against existing open orders to see whether it can be matched.
     * Two orders match if they have opposing directions, matching RICs and quantities,
     * and if the sell price is less than or equal to the buy price
     *
     * @param newOrder
     * @return
     */
    public boolean checkOrderMatch(Order newOrder) {
        List<Order> equalMatchingOrders = filterEqualMatchingOrders(newOrder);
        if (equalMatchingOrders.size() > 0) {
            // If there are multiple matching orders at the same price for a new order,
            // it should be matched against the earliest matching existing orders
            Order earliestOrder = equalMatchingOrders.get(0);
            // Executed orders are removed from the set of open orders against which new orders can be matched
            System.out.println(">>>> Matching Earliest Order " + earliestOrder);
            StockExchangeUtils.changeOrderStatusAsExecuted(newOrder, earliestOrder);
            StockExchangeUtils.changeExecutionPrice(newOrder, earliestOrder);
            return true;
        }

        List<Order> otherMatchingOrders = filterOtherMatchingOrders(newOrder);
        if (otherMatchingOrders.size() > 1) {
            if (newOrder.getDirection().equals(Direction.SELL)) {
                // If there are multiple matching orders at different prices for a new sell order,
                // it should be matched against the order with the highest price
                Collections.sort(otherMatchingOrders, StockExchangeUtils.getPriceComparator());
                Order highestPriceOrder = otherMatchingOrders.get(otherMatchingOrders.size() - 1);
                System.out.println(">>> Match is : " + highestPriceOrder);
                // Executed orders are removed from the set of open orders against which new orders can be matched
                StockExchangeUtils.changeOrderStatusAsExecuted(newOrder, highestPriceOrder);
                // When two orders are matched they are said to be ‘executed’,
                // and the price at which they are executed (the execution price) is the price of the newly added order
                StockExchangeUtils.changeExecutionPrice(newOrder, highestPriceOrder);
                return true;
            }

            if (newOrder.getDirection().equals(Direction.BUY)) {
                // If there are multiple matching orders at different prices for a new buy order,
                // it should be matched against the order with the lowest price
                Collections.sort(otherMatchingOrders, StockExchangeUtils.getPriceComparator());
                Order lowestPriceOrder = otherMatchingOrders.get(0);
                System.out.println(">>> Match is : " + lowestPriceOrder);
                // Executed orders are removed from the set of open orders against which new orders can be matched
                StockExchangeUtils.changeOrderStatusAsExecuted(newOrder, lowestPriceOrder);
                // When two orders are matched they are said to be ‘executed’,
                // and the price at which they are executed (the execution price) is the price of the newly added order
                StockExchangeUtils.changeExecutionPrice(newOrder, lowestPriceOrder);
                return true;
            }
        }
        return false;
    }


    public List<Order> filterEqualMatchingOrders(final Order newOrder) {
        List<Order> matchingEqualOrders = orders.stream()
                .filter(order -> StockExchangeUtils.orderIsOpen(order))
                .filter(order -> StockExchangeUtils.ricMatches(order, newOrder))
                .filter(order -> StockExchangeUtils.oppositeDirections(order, newOrder))
                .filter(order -> StockExchangeUtils.quantitiesMatches(order, newOrder))
                .filter(order -> StockExchangeUtils.sellPriceIsEqualToBuyPrice(order, newOrder))
                .collect(Collectors.toList());
        return matchingEqualOrders;
    }


    public List<Order> filterOtherMatchingOrders(Order newOrder) {
        List<Order> otherMatchingOrders = orders.stream()
                .filter(order -> StockExchangeUtils.orderIsOpen(order))
                .filter(order -> StockExchangeUtils.ricMatches(order, newOrder))
                .filter(order -> StockExchangeUtils.oppositeDirections(order, newOrder))
                .filter(order -> StockExchangeUtils.quantitiesMatches(order, newOrder))
                .filter(order -> StockExchangeUtils.sellPriceIsLessThanBuyPrice(order, newOrder))
                .collect(Collectors.toList());

        return otherMatchingOrders;
    }

    /**
     * Provide open interest for a given RIC and direction
     * Open interest is the total quantity of all open orders for the given RIC and direction at each price point
     *
     * @param ric
     * @param buy
     * @return
     */
    public Map<BigDecimal, Integer> provideOpenInterestForGivenRicAndDirection(RIC ric, Direction buy) {
        Stream<Order> openOrderStream = orders.stream()
                .filter(order -> order.getRic().equals(ric))
                .filter(order -> order.getStatus().equals(Status.OPEN))
                .filter(order -> order.getDirection().equals(buy));

        Map<BigDecimal, Integer> totalQuantityOfOpenOrders = new HashMap<BigDecimal, Integer>();

        openOrderStream.forEach(order -> {
            if (totalQuantityOfOpenOrders.containsKey(order.getPrice())) {
                Integer quantity = totalQuantityOfOpenOrders.get(order.getPrice());
                totalQuantityOfOpenOrders.put(order.getPrice(), new Integer(quantity + 1));
            } else {
                totalQuantityOfOpenOrders.put(order.getPrice(), new Integer(1));
            }
        });

        System.out.println("Open Interest = " + totalQuantityOfOpenOrders);
        return totalQuantityOfOpenOrders;
    }

    /**
     * Provide the average execution price for a given RIC
     * The average execution price is the average price per unit of all executions for the given RIC
     *
     * @param ric
     * @return
     */
    public BigDecimal averageExecutionPriceForAGivenRIC(RIC ric) {
        final double[] totalPrice = {0};
        final int[] totalQuantity = {0};
        Stream<Order> orderStream = orders.stream()
                .filter(order -> order.getRic().equals(ric))
                .filter(order -> order.getStatus().equals(Status.EXECUTED));

        orderStream.forEach(order -> {
            totalPrice[0] = totalPrice[0] + (order.getExecutionPrice().doubleValue() * order.getQuantity());
            totalQuantity[0] = totalQuantity[0] + order.getQuantity();

        });

        BigDecimal averageExecutionPrice = new BigDecimal(totalPrice[0] / totalQuantity[0]).setScale(2, RoundingMode.CEILING);
        System.out.println("Average Execution Price = " + averageExecutionPrice);
        return averageExecutionPrice;
    }


    /**
     * Provide executed quantity for a given RIC and user
     * Executed quantity is the sum of quantities of executed orders for the given RIC and user.
     * The quantity of sell orders should be negated
     *
     * @param ric
     * @param userName
     * @return
     */
    public int findExecutedQuantityForAnRICAndAnUser(RIC ric, String userName) {
        final int[] totalBuyQuantity = {0};
        final int[] totalSellQuantity = {0};

        Stream<Order> orderStream = orders.stream()
                .filter(order -> order.getRic().equals(ric))
                .filter(order -> order.getUser().getName().equals(userName))
                .filter(order -> order.getStatus().equals(Status.EXECUTED));

        orderStream.forEach(order -> {
            if (order.getDirection().equals(Direction.BUY)) {
                totalBuyQuantity[0] = totalBuyQuantity[0] + order.getQuantity();
            }
            if (order.getDirection().equals(Direction.SELL)) {
                totalSellQuantity[0] = totalSellQuantity[0] + order.getQuantity();
            }
        });

        int executedQuantity = totalBuyQuantity[0] - totalSellQuantity[0];
        System.out.println("Executed Quantity : " + executedQuantity);
        return executedQuantity;
    }

}