package com.credit.suisse.utils;

import com.credit.suisse.domain.Direction;
import com.credit.suisse.domain.Order;
import com.credit.suisse.domain.Status;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 * Created by harish on 01/11/15.
 */
public class StockExchangeUtils {

    public static void changeExecutionPrice(Order newOrder, Order matchingOrder) {
        newOrder.setExecutionPrice(matchingOrder.getPrice());
        matchingOrder.setExecutionPrice(matchingOrder.getPrice());
    }

    public static boolean ricMatches(Order order, Order newOrder) {
        return order.getRic().equals(newOrder.getRic());
    }

    public static boolean orderIsOpen(Order order) {
        return order.getStatus().equals(Status.OPEN);
    }

    public static void changeOrderStatusAsExecuted(Order newOrder, Order order) {
        order.setStatus(Status.EXECUTED);
        newOrder.setStatus(Status.EXECUTED);
    }


    public static boolean sellPriceIsEqualToBuyPrice(Order order, Order newOrder) {
        BigDecimal sellPrice = extractSellPrice(order, newOrder);
        BigDecimal buyPrice = extractBuyPrice(order, newOrder);
        return sellPrice.doubleValue() == buyPrice.doubleValue();
    }

    public static boolean sellPriceIsLessThanBuyPrice(Order order, Order newOrder) {
        BigDecimal sellPrice = extractSellPrice(order, newOrder);
        BigDecimal buyPrice = extractBuyPrice(order, newOrder);
        return sellPrice.doubleValue() < buyPrice.doubleValue();
    }

    public static BigDecimal extractBuyPrice(Order order, Order newOrder) {
        if (order.getDirection().equals(Direction.BUY)) {
            return order.getPrice();
        } else if (newOrder.getDirection().equals(Direction.BUY)) {
            return newOrder.getPrice();
        }
        return null;
    }

    public static BigDecimal extractSellPrice(Order order, Order newOrder) {
        if (order.getDirection().equals(Direction.SELL)) {
            return order.getPrice();
        } else if (newOrder.getDirection().equals(Direction.SELL)) {
            return newOrder.getPrice();
        }
        return null;
    }

    public static boolean quantitiesMatches(Order order, Order newOrder) {
        return order.getQuantity() == newOrder.getQuantity();
    }

    public static boolean oppositeDirections(Order order, Order newOrder) {
        if (order.getDirection() == newOrder.getDirection()) {
            return false;
        }
        return true;
    }

    public static Comparator<Order> getPriceComparator() {
        return new Comparator<Order>() {
            @Override
            public int compare(Order o1, Order o2) {
                return Double.valueOf(o1.getPrice().doubleValue() - o2.getPrice().doubleValue()).intValue();
            }
        };
    }
}
