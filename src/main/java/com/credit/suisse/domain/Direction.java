package com.credit.suisse.domain;

/**
 * Created by harish on 29/10/15.
 */
public enum Direction {
    BUY,
    SELL
}
