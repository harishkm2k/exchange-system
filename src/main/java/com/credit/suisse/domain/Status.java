package com.credit.suisse.domain;

/**
 * Created by harish on 30/10/15.
 */
public enum Status {
    OPEN,
    EXECUTED
}
