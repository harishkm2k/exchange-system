package com.credit.suisse.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by harish on 29/10/15.
 * <p>
 * An order consists of direction (buy/sell), RIC (Reuters Instrument Code), quantity, price and user.
 */
public class Order {
    private Direction direction;
    private int quantity;
    private RIC ric;
    private BigDecimal price;
    private BigDecimal executionPrice;
    private User user;
    private Status status;

    public Order(Direction direction, int quantity, RIC ric, BigDecimal price, User user) {
        this.direction = direction;
        this.quantity = quantity;
        this.ric = ric;
        this.price = price;
        this.user = user;
        this.status = Status.OPEN;
    }

    public Direction getDirection() {
        return direction;
    }


    public BigDecimal getPrice() {
        return price;
    }


    public int getQuantity() {
        return quantity;
    }


    public RIC getRic() {
        return ric;
    }


    public User getUser() {
        return user;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public BigDecimal getExecutionPrice() {
        return executionPrice;
    }

    public void setExecutionPrice(BigDecimal executionPrice) {
        this.executionPrice = executionPrice;
    }

    @Override
    public String toString() {
        return direction + " " + quantity + " " + ric + " @ " + price.setScale(2, RoundingMode.CEILING) + " " + user + " " + status;
    }
}
