package com.credit.suisse;

import com.credit.suisse.domain.*;
import com.credit.suisse.stock.StockExchange;

import java.math.BigDecimal;

/**
 * Created by harish on 30/10/15.
 */
public class Main {

    public static void main(String... args) {

        StockExchange stockExchange = new StockExchange();

        // SCENARIO 1
        System.out.println("---------------------- SCENARIO 1 ----------------------------------------");
        Order order1 = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        stockExchange.addOrder(order1);
        stockExchange.checkOrderMatch(order1);

        Order order2 = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User2"));
        stockExchange.addOrder(order2);
        stockExchange.checkOrderMatch(order2);

        // SCENARIO 2
        System.out.println("---------------------- SCENARIO 2 ----------------------------------------");
        Order order3 = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User3"));
        stockExchange.addOrder(order3);
        stockExchange.checkOrderMatch(order3);

        Order order4 = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User4"));
        stockExchange.addOrder(order4);
        stockExchange.checkOrderMatch(order4);

        Order order5 = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User5"));
        stockExchange.addOrder(order5);
        stockExchange.checkOrderMatch(order5);

        Order order6 = new Order(Direction.BUY, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User6"));
        stockExchange.addOrder(order6);
        stockExchange.checkOrderMatch(order6);


        // SCENARIO 3
        System.out.println("---------------------- SCENARIO 3 ----------------------------------------");
        Order order7 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(11), new User("User7"));
        stockExchange.addOrder(order7);
        stockExchange.checkOrderMatch(order7);

        Order order8 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(12), new User("User8"));
        stockExchange.addOrder(order8);
        stockExchange.checkOrderMatch(order8);

        Order order9 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(13), new User("User9"));
        stockExchange.addOrder(order9);
        stockExchange.checkOrderMatch(order9);

        Order order10 = new Order(Direction.SELL, 100, RIC.CSGN_VX, new BigDecimal(10), new User("User10"));
        stockExchange.addOrder(order10);
        stockExchange.checkOrderMatch(order10);

        // SCENARIO 4
        System.out.println("---------------------- SCENARIO 4 ----------------------------------------");
        Order order11 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(30), new User("User11"));
        stockExchange.addOrder(order11);
        stockExchange.checkOrderMatch(order11);

        Order order12 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(25), new User("User12"));
        stockExchange.addOrder(order12);
        stockExchange.checkOrderMatch(order12);

        Order order13 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(20), new User("User13"));
        stockExchange.addOrder(order13);
        stockExchange.checkOrderMatch(order13);

        Order order14 = new Order(Direction.BUY, 200, RIC.CSGN_VX, new BigDecimal(35), new User("User14"));
        stockExchange.addOrder(order14);
        stockExchange.checkOrderMatch(order14);

        System.out.println("--------------------------------------------------------------------------");
        for (Order order : stockExchange.getOrders()) {
            System.out.println("order = " + order);
        }
        System.out.println("--------------------------------------------------------------------------");

        // SCENARIO 5
        System.out.println("---------------------- SCENARIO 5 ----------------------------------------");
        // Provide open interest for a given RIC and direction
        // Open interest is the total quantity of all open orders for the given RIC and direction at each price point
        Order order15 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(12), new User("User15"));
        stockExchange.addOrder(order15);
        stockExchange.provideOpenInterestForGivenRicAndDirection(RIC.CSGN_VX, Direction.BUY);

        // SCENARIO 6
        System.out.println("---------------------- SCENARIO 6 ----------------------------------------");
        // Provide the average execution price for a given RIC
        // The average execution price is the average price per unit of all executions for the given RIC
        stockExchange.averageExecutionPriceForAGivenRIC(RIC.CSGN_VX);


        // SCENARIO 7
        System.out.println("---------------------- SCENARIO 7 ----------------------------------------");
        // Provide executed quantity for a given RIC and user
        // Executed quantity is the sum of quantities of executed orders for the given RIC and user.
        // The quantity of sell orders should be negated

        Order order16 = new Order(Direction.BUY, 200, RIC.CSGN_VX, new BigDecimal(35), new User("User14"));
        order16.setStatus(Status.EXECUTED);
        stockExchange.addOrder(order16);

        Order order17 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(35), new User("User14"));
        order17.setStatus(Status.EXECUTED);
        stockExchange.addOrder(order17);

        stockExchange.findExecutedQuantityForAnRICAndAnUser(RIC.CSGN_VX, "User14");

    }

}
