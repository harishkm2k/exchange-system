package com.credit.suisse.utils;

import com.credit.suisse.domain.*;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 * Created by harish on 01/11/15.
 */
public class StockExchangeUtilsTest {

    @Test
    public void shouldChangeExecutionPrice() {
        // Setup
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order matchingOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(200.2), new User("User2"));

        // Run
        StockExchangeUtils.changeExecutionPrice(newOrder, matchingOrder);

        // Verify
        Assert.assertEquals(newOrder.getExecutionPrice(), matchingOrder.getPrice());
        Assert.assertEquals(matchingOrder.getExecutionPrice(), matchingOrder.getPrice());

    }

    @Test
    public void shouldNotMatchRic() {
        // Setup
        Order newOrder = new Order(Direction.SELL, 1000, RIC.CSGN_VX, new BigDecimal(100.2), new User("User1"));
        Order matchingOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(200.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.ricMatches(newOrder, matchingOrder);

        // Verify
        Assert.assertFalse(returned);
    }

    @Test
    public void shouldMatchRic() {
        // Setup
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order matchingOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(200.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.ricMatches(newOrder, matchingOrder);

        // Verify
        Assert.assertTrue(returned);
    }

    @Test
    public void shouldCheckIfOrderIsOpen() {
        // Setup
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));

        // Run
        boolean returned = StockExchangeUtils.orderIsOpen(newOrder);

        // Verify
        Assert.assertTrue(returned);
    }

    @Test
    public void shouldChangeOrderStatusAsExecuted() {
        // Setup
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order matchingOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(200.2), new User("User2"));

        // Run
        StockExchangeUtils.changeOrderStatusAsExecuted(newOrder, matchingOrder);

        // Verify
        Assert.assertEquals(Status.EXECUTED, newOrder.getStatus());
        Assert.assertEquals(Status.EXECUTED, matchingOrder.getStatus());
    }

    @Test
    public void shouldNotCheckSellPriceIsEqualToBuyPrice() {
        // Setup
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order matchingOrder = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(200.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.sellPriceIsEqualToBuyPrice(newOrder, matchingOrder);

        // Verify
        Assert.assertFalse(returned);
    }

    @Test
    public void shouldCheckSellPriceIsEqualToBuyPrice() {
        // Setup
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order matchingOrder = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.sellPriceIsEqualToBuyPrice(newOrder, matchingOrder);

        // Verify
        Assert.assertTrue(returned);
    }

    @Test
    public void shouldNotHaveSellPriceLessThanBuyPrice() {
        // Setup
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(150.2), new User("User1"));
        Order matchingOrder = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.sellPriceIsLessThanBuyPrice(newOrder, matchingOrder);

        // Verify
        Assert.assertFalse(returned);
    }

    @Test
    public void shouldHaveSellPriceLessThanBuyPrice() {
        // Setup
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order matchingOrder = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(150.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.sellPriceIsLessThanBuyPrice(newOrder, matchingOrder);

        // Verify
        Assert.assertTrue(returned);
    }

    @Test
    public void shouldExtractBuyPriceFromOrder() {
        // Setup
        Order order = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(150.2), new User("User2"));

        // Run
        BigDecimal returned = StockExchangeUtils.extractBuyPrice(order, newOrder);

        // Verify
        Assert.assertEquals(new BigDecimal(100.2), returned);
    }

    @Test
    public void shouldExtractBuyPriceFromNewOrder() {
        // Setup
        Order order = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order newOrder = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(150.2), new User("User2"));

        // Run
        BigDecimal returned = StockExchangeUtils.extractBuyPrice(order, newOrder);

        // Verify
        Assert.assertEquals(new BigDecimal(150.2), returned);
    }

    @Test
    public void shouldExtractSellPriceFromOrder() {
        // Setup
        Order order = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order newOrder = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(150.2), new User("User2"));

        // Run
        BigDecimal returned = StockExchangeUtils.extractSellPrice(order, newOrder);

        // Verify
        Assert.assertEquals(new BigDecimal(100.2), returned);
    }

    @Test
    public void shouldExtractSellPriceFromNewOrder() {
        // Setup
        Order order = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(150.2), new User("User2"));

        // Run
        BigDecimal returned = StockExchangeUtils.extractSellPrice(order, newOrder);

        // Verify
        Assert.assertEquals(new BigDecimal(150.2), returned);
    }

    @Test
    public void shouldNotHaveQuantitiesMatches() {
        // Setup
        Order order = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order newOrder = new Order(Direction.SELL, 2000, RIC.VOD_L, new BigDecimal(150.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.quantitiesMatches(order, newOrder);

        // Verify
        Assert.assertFalse(returned);
    }

    @Test
    public void shouldQuantitiesMatches() {
        // Setup
        Order order = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(150.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.quantitiesMatches(order, newOrder);

        // Verify
        Assert.assertTrue(returned);
    }

    @Test
    public void shouldNotHaveOppositeDirections() {
        // Setup
        // Setup
        Order order = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order newOrder = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(150.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.oppositeDirections(order, newOrder);

        // Verify
        Assert.assertFalse(returned);
    }

    @Test
    public void shouldHaveOppositeDirections() {
        // Setup
        Order order = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(150.2), new User("User2"));

        // Run
        boolean returned = StockExchangeUtils.oppositeDirections(order, newOrder);

        // Verify
        Assert.assertTrue(returned);
    }

    @Test
    public void testGetPriceComparator() {
        //Setup
        Order order = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(200), new User("User1"));
        Order newOrder = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100), new User("User2"));

        // Run
        Comparator<Order> comparator = StockExchangeUtils.getPriceComparator();
        int returned = comparator.compare(order, newOrder);

        // Verify
        Assert.assertNotNull(comparator);
        Assert.assertEquals("com.credit.suisse.utils.StockExchangeUtils$1", comparator.getClass().getName());
        Assert.assertEquals(100, returned);
    }
}