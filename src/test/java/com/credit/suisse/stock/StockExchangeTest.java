package com.credit.suisse.stock;

import com.credit.suisse.domain.*;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by harish on 30/10/15.
 */
public class StockExchangeTest {

    @Test
    public void shouldAddOrdersToStockExchange() {
        Order orderOne = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        Order orderTwo = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User2"));

        StockExchange stockExchange = new StockExchange();
        stockExchange.addOrder(orderOne);
        stockExchange.addOrder(orderTwo);

        // Run
        ArrayList<Order> returned = stockExchange.getOrders();

        // Verify
        Assert.assertEquals(2, returned.size());
        Assert.assertSame(orderOne, returned.get(0));
        Assert.assertSame(orderTwo, returned.get(1));

    }


    @Test
    public void shouldCheckEqualMatchingOrders() {
        // Setup
        StockExchange stockExchange = new StockExchange();
        System.out.println("---------------------- SCENARIO 1 ----------------------------------------");
        Order order1 = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        stockExchange.addOrder(order1);
        stockExchange.checkOrderMatch(order1);

        Order order2 = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User2"));
        stockExchange.addOrder(order2);
        stockExchange.checkOrderMatch(order2);

        // Verify
        Assert.assertEquals(2, stockExchange.getOrders().size());
        Assert.assertEquals(Status.EXECUTED, stockExchange.getOrders().get(0).getStatus());
        Assert.assertEquals(Status.EXECUTED, stockExchange.getOrders().get(1).getStatus());
        Assert.assertEquals(order1.getPrice(), stockExchange.getOrders().get(0).getExecutionPrice());
        Assert.assertEquals(order2.getPrice(), stockExchange.getOrders().get(1).getExecutionPrice());
    }


    @Test
    public void filterEqualMatchingOrders() {
        // Setup
        StockExchange stockExchange = new StockExchange();
        System.out.println("---------------------- SCENARIO 2 ----------------------------------------");
        Order orderThree = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User3"));
        stockExchange.addOrder(orderThree);

        Order orderFour = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User4"));
        stockExchange.addOrder(orderFour);

        Order orderFive = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User5"));
        stockExchange.addOrder(orderFive);

        Order orderSix = new Order(Direction.BUY, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User6"));
        stockExchange.addOrder(orderSix);

        // Run
        ArrayList<Order> returned = stockExchange.filterEqualMatchingOrders(orderSix);

        // Verify
        Assert.assertEquals(3, returned.size());
        Assert.assertEquals(orderThree.getPrice(), returned.get(0).getPrice());

    }

    @Test
    public void shouldFilterOtherMatchingOrders() {
        // Setup
        StockExchange stockExchange = new StockExchange();
        System.out.println("---------------------- SCENARIO 3 ----------------------------------------");
        Order orderSeven = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(11), new User("User7"));
        stockExchange.addOrder(orderSeven);

        Order order8 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(12), new User("User8"));
        stockExchange.addOrder(order8);

        Order order9 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(13), new User("User9"));
        stockExchange.addOrder(order9);

        Order order10 = new Order(Direction.SELL, 100, RIC.CSGN_VX, new BigDecimal(10), new User("User10"));
        stockExchange.addOrder(order10);

        // Run
        ArrayList<Order> returned = stockExchange.filterOtherMatchingOrders(order10);

        // Verify
        Assert.assertEquals(3, returned.size());
        Assert.assertEquals(orderSeven.getPrice(), returned.get(0).getPrice());

    }


    @Test
    public void shouldCheckEqualMatchingEarliestOrders() {
        // Setup
        StockExchange stockExchange = new StockExchange();
        System.out.println("---------------------- SCENARIO 2 ----------------------------------------");
        Order orderThree = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User3"));
        stockExchange.addOrder(orderThree);
        stockExchange.checkOrderMatch(orderThree);

        Order orderFour = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User4"));
        stockExchange.addOrder(orderFour);
        stockExchange.checkOrderMatch(orderFour);

        Order orderFive = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User5"));
        stockExchange.addOrder(orderFive);
        stockExchange.checkOrderMatch(orderFive);

        Order orderSix = new Order(Direction.BUY, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User6"));
        stockExchange.addOrder(orderSix);
        stockExchange.checkOrderMatch(orderSix);

        // Verify
        Assert.assertEquals(4, stockExchange.getOrders().size());
        Assert.assertEquals(Status.EXECUTED, stockExchange.getOrders().get(0).getStatus());
        Assert.assertEquals(Status.OPEN, stockExchange.getOrders().get(1).getStatus());
        Assert.assertEquals(Status.OPEN, stockExchange.getOrders().get(2).getStatus());
        Assert.assertEquals(Status.EXECUTED, stockExchange.getOrders().get(3).getStatus());
        Assert.assertEquals(orderThree.getExecutionPrice(), stockExchange.getOrders().get(0).getExecutionPrice());
        Assert.assertEquals(orderSix.getExecutionPrice(), stockExchange.getOrders().get(3).getExecutionPrice());
    }

    @Test
    public void forNewSellOrderCheckOtherMatchingOrdersWithHighPrice() {
        // Setup
        StockExchange stockExchange = new StockExchange();
        System.out.println("---------------------- SCENARIO 3 ----------------------------------------");
        Order orderSeven = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(11), new User("User7"));
        stockExchange.addOrder(orderSeven);
        stockExchange.checkOrderMatch(orderSeven);

        Order order8 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(12), new User("User8"));
        stockExchange.addOrder(order8);
        stockExchange.checkOrderMatch(order8);

        Order order9 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(13), new User("User9"));
        stockExchange.addOrder(order9);
        stockExchange.checkOrderMatch(order9);

        Order order10 = new Order(Direction.SELL, 100, RIC.CSGN_VX, new BigDecimal(10), new User("User10"));
        stockExchange.addOrder(order10);
        stockExchange.checkOrderMatch(order10);

        // Verify
        Assert.assertEquals(4, stockExchange.getOrders().size());
        Assert.assertEquals(Status.OPEN, stockExchange.getOrders().get(0).getStatus());
        Assert.assertEquals(Status.OPEN, stockExchange.getOrders().get(1).getStatus());
        Assert.assertEquals(Status.EXECUTED, stockExchange.getOrders().get(2).getStatus());
        Assert.assertEquals(Status.EXECUTED, stockExchange.getOrders().get(3).getStatus());
        Assert.assertEquals(order9.getExecutionPrice(), stockExchange.getOrders().get(2).getExecutionPrice());
        Assert.assertEquals(order10.getExecutionPrice(), stockExchange.getOrders().get(3).getExecutionPrice());
    }


    @Test
    public void forNewBuyOrderCheckOtherMatchingOrdersWithLowPrice() {
        // Setup
        StockExchange stockExchange = new StockExchange();
        System.out.println("---------------------- SCENARIO 4 ----------------------------------------");
        Order order11 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(30), new User("User11"));
        stockExchange.addOrder(order11);
        stockExchange.checkOrderMatch(order11);

        Order order12 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(25), new User("User12"));
        stockExchange.addOrder(order12);
        stockExchange.checkOrderMatch(order12);

        Order order13 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(20), new User("User13"));
        stockExchange.addOrder(order13);
        stockExchange.checkOrderMatch(order13);

        Order order14 = new Order(Direction.BUY, 200, RIC.CSGN_VX, new BigDecimal(35), new User("User14"));
        stockExchange.addOrder(order14);
        stockExchange.checkOrderMatch(order14);

        // Verify
        Assert.assertEquals(4, stockExchange.getOrders().size());
        Assert.assertEquals(Status.OPEN, stockExchange.getOrders().get(0).getStatus());
        Assert.assertEquals(Status.OPEN, stockExchange.getOrders().get(1).getStatus());
        Assert.assertEquals(Status.EXECUTED, stockExchange.getOrders().get(2).getStatus());
        Assert.assertEquals(Status.EXECUTED, stockExchange.getOrders().get(3).getStatus());
        Assert.assertEquals(order13.getExecutionPrice(), stockExchange.getOrders().get(2).getExecutionPrice());
        Assert.assertEquals(order14.getExecutionPrice(), stockExchange.getOrders().get(2).getExecutionPrice());
    }

    @Test
    public void shouldProvideOpenInterestForGivenRICAndDirection() {
        // Setup
        StockExchange stockExchange = new StockExchange();

        populate(stockExchange);

        // Run
        Map<BigDecimal, Integer> returned = stockExchange.provideOpenInterestForGivenRicAndDirection(RIC.CSGN_VX, Direction.BUY);


        // Verify
        Assert.assertEquals(15, stockExchange.getOrders().size());
        Assert.assertEquals(2, returned.size());
        Assert.assertEquals("{12=2, 11=1}", returned.toString());
    }


    @Test
    public void shouldGetAverageExecutionTime() {
        // Setup
        StockExchange stockExchange = new StockExchange();

        populate(stockExchange);

        // Run
        BigDecimal returned = stockExchange.averageExecutionPriceForAGivenRIC(RIC.CSGN_VX);


        // Verify
        Assert.assertEquals(15, stockExchange.getOrders().size());
        Assert.assertEquals(new BigDecimal(11.44).setScale(2, RoundingMode.CEILING), returned);
    }

    @Test
    public void shouldGetExecutedQuantity() {
        // Setup
        StockExchange stockExchange = new StockExchange();

        populate(stockExchange);

        Order order16 = new Order(Direction.BUY, 200, RIC.CSGN_VX, new BigDecimal(35), new User("User14"));
        order16.setStatus(Status.EXECUTED);
        stockExchange.addOrder(order16);

        Order order17 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(35), new User("User14"));
        order17.setStatus(Status.EXECUTED);
        stockExchange.addOrder(order17);

        // Run
        int returned = stockExchange.findExecutedQuantityForAnRICAndAnUser(RIC.CSGN_VX, "User14");


        // Verify
        Assert.assertEquals(17, stockExchange.getOrders().size());
        Assert.assertEquals(200, returned);
    }

    private void populate(StockExchange stockExchange) {
        Order order1 = new Order(Direction.SELL, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User1"));
        stockExchange.addOrder(order1);
        stockExchange.checkOrderMatch(order1);

        Order order2 = new Order(Direction.BUY, 1000, RIC.VOD_L, new BigDecimal(100.2), new User("User2"));
        stockExchange.addOrder(order2);
        stockExchange.checkOrderMatch(order2);

        Order order3 = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User3"));
        stockExchange.addOrder(order3);
        stockExchange.checkOrderMatch(order3);

        Order order4 = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User4"));
        stockExchange.addOrder(order4);
        stockExchange.checkOrderMatch(order4);

        Order order5 = new Order(Direction.SELL, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User5"));
        stockExchange.addOrder(order5);
        stockExchange.checkOrderMatch(order5);

        Order order6 = new Order(Direction.BUY, 2000, RIC.CSGN_VX, new BigDecimal(10.5), new User("User6"));
        stockExchange.addOrder(order6);
        stockExchange.checkOrderMatch(order6);

        Order order7 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(11), new User("User7"));
        stockExchange.addOrder(order7);
        stockExchange.checkOrderMatch(order7);

        Order order8 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(12), new User("User8"));
        stockExchange.addOrder(order8);
        stockExchange.checkOrderMatch(order8);

        Order order9 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(13), new User("User9"));
        stockExchange.addOrder(order9);
        stockExchange.checkOrderMatch(order9);

        Order order10 = new Order(Direction.SELL, 100, RIC.CSGN_VX, new BigDecimal(10), new User("User10"));
        stockExchange.addOrder(order10);
        stockExchange.checkOrderMatch(order10);

        Order order11 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(30), new User("User11"));
        stockExchange.addOrder(order11);
        stockExchange.checkOrderMatch(order11);

        Order order12 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(25), new User("User12"));
        stockExchange.addOrder(order12);
        stockExchange.checkOrderMatch(order12);

        Order order13 = new Order(Direction.SELL, 200, RIC.CSGN_VX, new BigDecimal(20), new User("User13"));
        stockExchange.addOrder(order13);
        stockExchange.checkOrderMatch(order13);

        Order order14 = new Order(Direction.BUY, 200, RIC.CSGN_VX, new BigDecimal(35), new User("User14"));
        stockExchange.addOrder(order14);
        stockExchange.checkOrderMatch(order14);

        Order order15 = new Order(Direction.BUY, 100, RIC.CSGN_VX, new BigDecimal(12), new User("User15"));
        stockExchange.addOrder(order15);
    }

}