The code is available here in a git repo :

https://bitbucket.org/harishkm2k/exchange-system

The Test coverage is 100%

IMPLEMENTATION 1 : The First iteration is done using the Java 5 Approach.

IMPLEMENTATION 2 : The Second iteration can be done using Java 8 Streams, Filter, Map & Lambda functions.

TODO : The third iteration can be done using Parallel stream and optimising it further

Moreover, we can use the Java 8 Parallel Stream to improve the performance for parallel execution.

Regards
Harish
https://uk.linkedin.com/in/harishkm2k